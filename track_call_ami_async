#!/usr/bin/python3
#+
# This script originates a call and monitors events to
# determine the status of the number dialled. This version
# uses AMI.
#
# Invoke as follows:
#
#     track_call --user=«username» --password=«password» [--debug] \
#          «inchannel» «outcontext» «outnumber»
#
# Note: this code may seem a bit convoluted and redundant in places.
# This is because I have seen events that I expected to appear at
# certain points fail to appear under certain situations. So I try
# to cope with that. This may be to do with the different versions
# of Asterisk I have used over the years. Anyway, feel free to
# experiment.
#-

import sys
import time
import math
import signal
import asyncio
import getopt
import traceback
import seaskirt

debug = False
username = password = None
event_timeout = 30 # how long to wait for next significant event
hangup_detect_all = False
opts, args = getopt.getopt \
  (
    sys.argv[1:],
    "",
    ["debug", "hangup-detect-all", "password=", "user="]
  )
for keyword, value in opts :
    if keyword == "--debug" :
        debug = True
    elif keyword == "--password" :
        password = value
    elif keyword == "--hangup-detect-all" :
        hangup_detect_all = True
    elif keyword == "--user" :
        username = value
    #end if
#end for
if username == None or password == None :
    raise getopt.GetoptError("need --user and --password")
#end if
if len(args) != 3 :
    raise getopt.GetoptError("usage: %s «inchannel» «outcontext» «outnumber»" % sys.argv[0])
#end if
inchannel, outcontext, outnumber = args

run_done = None

def logline(msg) :
    now = time.time()
    now_ts = time.gmtime(math.floor(now))
    now_frac = now - math.floor(now)
    timestamp = \
        (
            "%0.4d%0.2d%0.2d-%0.2d%0.2d%06.3fZ"
        %
            (
                now_ts.tm_year, now_ts.tm_mon, now_ts.tm_mday,
                now_ts.tm_hour, now_ts.tm_min, now_ts.tm_sec + now_frac,
            )
        )
    sys.stderr.write("%s: %s\n" % (timestamp, msg))
#end logline

def set_run_done() :
    if not run_done.done() :
        run_done.set_result(None)
    #end if
#end set_run_done

def interrupted(signum, frame) :
    if run_done != None :
        sys.stderr.write("setting run_done\n")
        run_done.get_loop().call_soon_threadsafe(set_run_done)
    #end if
    # raise KeyboardInterrupt("somebody killed me")
#end interrupted

signal.signal(signal.SIGTERM, interrupted) # kill(1) command
signal.signal(signal.SIGINT, interrupted) # user hit CTRL/C

async def main() :
    try :
        global run_done
        loop = asyncio.get_running_loop()
        run_done = loop.create_future()
        manager = await seaskirt.ManagerAsync \
          (
            username = username,
            password = password,
            want_events = True,
            debug = debug
          )
        call_id = None
        outdevice = None
        logline("Calling %s on %s" % (outnumber, outcontext))
        response = await manager.send_request \
          (
            action = "Originate",
            parms =
                {
                    "Channel" : inchannel,
                    "Context" : outcontext,
                    "Exten" : outnumber,
                    "Priority" : 1,
                    "Async" : "true",
                }
          )
        awaiting_response = None
        while True :
            if awaiting_response == None :
                awaiting_response = asyncio.create_task(manager.get_response())
                response_timeout_start = time.time()
            #end if
            await asyncio.wait \
              (
                [awaiting_response, run_done],
                return_when = asyncio.FIRST_COMPLETED
              )
            if run_done.done() :
                break
            if (
                    not awaiting_response.done()
                and
                    time.time() - response_timeout_start > event_timeout
            ) :
                logline("No response from Manager in %s seconds" % event_timeout)
                break
            #end if
            if awaiting_response.done() :
                response = await awaiting_response
                awaiting_response = None
                if debug :
                    logline("event: %s" % response)
                #end if
                if "Response" in response :
                    if response["Response"] != "Success" :
                        logline("Originate failed: %s" % response["Message"])
                        set_run_done()
                        break
                    #end if
                elif "Event" in response :
                    if call_id == None :
                        if (
                                response["Event"] == "DialState"
                            and
                                response["DialStatus"] == "PROGRESS"
                            and
                                response["DestCallerIDNum"] == outnumber
                        ) :
                            call_id = response["DestLinkedid"]
                            logline("Progress: Call ID for %s = %s" % (outnumber, call_id))
                            outchannel = response["DestChannel"]
                            outdevice = outchannel[:outchannel.rindex("-")]
                            logline("Outchannel = %s, Outdevice = %s" % (outchannel, outdevice))
                        elif (
                            # older Asterisk?
                                    response["Event"]
                                in
                                        ("Hangup",)
                                    +
                                        ((), ("HangupRequest", "SoftHangupRequest"))
                                            [hangup_detect_all]
                            and
                                response["Exten"] == outnumber
                        ) :
                            logline \
                              (
                                    "Call for %s hanging up immediately with cause %s, text %s"
                                %
                                    (outnumber, response["Cause"], response.get("Cause-txt", "«none»"))
                              )
                            set_run_done()
                        #end if
                    elif "Linkedid" in response and response["Linkedid"] == call_id :
                      # Note that I will see multiple of these events with different Uniqueid,
                      # one for each channel that is part of this call.
                        if (
                            # older Asterisk?
                                response["Event"] == "DialEnd"
                            and
                                response["DialStatus"] == "NOANSWER"
                            and
                                response["DestCallerIDNum"] == outnumber
                        ) :
                            logline("No answer to call id %s" % call_id)
                            #set_run_done() # could stop monitoring here
                        elif response["Event"] == "BridgeEnter" :
                            logline("Call %s is connected" % call_id)
                        elif response["Event"] == "BridgeLeave" :
                            logline("Call %s is disconnected" % call_id)
                            set_run_done()
                        elif (
                                response["Event"]
                            in
                                    ("Hangup",)
                                +
                                    ((), ("HangupRequest", "SoftHangupRequest"))[hangup_detect_all]
                        ) :
                            logline("Call %s hung up with code %s" % (call_id, response["Cause"]))
                            set_run_done()
                        #end if
                    elif (
                            outdevice != None
                        and
                            response["Event"] == "DeviceStateChange"
                        and
                            response["Device"] == outdevice
                        and
                            response["State"] == "INUSE"
                    ) :
                        # Someone has already answered the call, without my seeing progress events
                        logline("Call to outdevice %s is connected" % outdevice)
                    #end if
                #end if
            #end if
        #end while
        if awaiting_response != None :
            awaiting_response.cancel()
        #end if
        await manager.close()
        logline("track_call ends")
    except Exception as err :
        sys.stderr.write("main fail %s\n" % repr(err))
        traceback.print_exc()
    #end try
#end main

asyncio.run(main())
